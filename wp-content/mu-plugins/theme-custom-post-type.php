<?php
/**
 * Plugin Name: Theme Custom Post Types
 * Description: This plugin adds the custom post type specific for this website template.
 * Version: 1.0.0
 * License: GPL2
 */

class themeACM {

    /**
     * Constructor. Called when plugin is initialised
     */
    function __construct() {
        add_action( 'init', array( $this, 'register_custom_post_type' ) );
    }

    function register_custom_post_type() {

        /**
         * Registers a Custom Post Type called Event
         */
        register_post_type('event', array(
            'labels' => array(
                'name'               => _x( 'Events', 'post type general name', 'sage' ),
                'singular_name'      => _x( 'Event', 'post type singular name', 'sage' ),
                'menu_name'          => _x( 'Events', 'admin menu', 'sage' ),
                'name_admin_bar'     => _x( 'Events', 'add new on admin bar', 'sage' ),
                'add_new'            => _x( 'Add New', 'Events', 'sage' ),
                'add_new_item'       => __( 'Add New Event', 'sage' ),
                'new_item'           => __( 'New Event', 'sage' ),
                'edit_item'          => __( 'Edit Event', 'sage' ),
                'view_item'          => __( 'View Event', 'sage' ),
                'all_items'          => __( 'All Events', 'sage' ),
                'search_items'       => __( 'Search Events', 'sage' ),
                'parent_item_colon'  => __( 'Parent Event:', 'sage' ),
                'not_found'          => __( 'No Events item found.', 'sage' ),
                'not_found_in_trash' => __( 'No Events item found in Trash.', 'sage' ),
            ),


            // Frontend
            'has_archive'        => true,
            'public'             => true,
            'hierarchical' => true,

            // Admin
            'capability_type' => 'post',
            'menu_icon'     => 'dashicons-tickets-alt',
            'menu_position' => 5,
            'query_var'     => true,
            'show_in_menu'  => true,
            'show_ui'       => true,
            'supports' => array( 'title', 'editor', 'thumbnail'),
        ));
        register_taxonomy(
            'event-category',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'event',        //post type name
            array(
                'hierarchical' => true, // true = category like, false = tag like
                'label' => __( 'Event Categories' ),
                'show_admin_column' => true, // display in admin column
                'public' => true, // false = make private
                'show_in_menu'  => true,
                'show_ui'       => true,
                //'rewrite'       => array('slug' => 'our-brands','with_front' => false),
                'labels' => array(
                    'add_new_item'       => __( 'Add New Category', 'sage' ),
                    'parent_item_colon'  => __( 'Parent Category:', 'sage' ),
                )
            )
        );

        /**
         * Registers a Custom Post Type called Event
         */
        register_post_type('venue', array(
            'labels' => array(
                'name'               => _x( 'Venues', 'post type general name', 'sage' ),
                'singular_name'      => _x( 'Venue', 'post type singular name', 'sage' ),
                'menu_name'          => _x( 'Venues', 'admin menu', 'sage' ),
                'name_admin_bar'     => _x( 'Venues', 'add new on admin bar', 'sage' ),
                'add_new'            => _x( 'Add New', 'Venues', 'sage' ),
                'add_new_item'       => __( 'Add New Venue', 'sage' ),
                'new_item'           => __( 'New Venue', 'sage' ),
                'edit_item'          => __( 'Edit Venue', 'sage' ),
                'view_item'          => __( 'View Venue', 'sage' ),
                'all_items'          => __( 'All Venues', 'sage' ),
                'search_items'       => __( 'Search Venues', 'sage' ),
                'parent_item_colon'  => __( 'Parent Venue:', 'sage' ),
                'not_found'          => __( 'No Venues item found.', 'sage' ),
                'not_found_in_trash' => __( 'No Venues item found in Trash.', 'sage' ),
            ),


            // Frontend
            'has_archive'        => true,
            'public'             => true,
            'hierarchical' => true,

            // Admin
            'capability_type' => 'post',
            'menu_icon'     => 'dashicons-palmtree',
            'menu_position' => 5,
            'query_var'     => true,
            'show_in_menu'  => true,
            'show_ui'       => true,
            'supports' => array( 'title', 'editor', 'thumbnail'),
        ));
        register_taxonomy(
            'venue-category',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'venue',        //post type name
            array(
                'hierarchical' => true, // true = category like, false = tag like
                'label' => __( 'Venue Categories' ),
                'show_admin_column' => true, // display in admin column
                'public' => true, // false = make private
                'show_in_menu'  => true,
                'show_ui'       => true,
                //'rewrite'       => array('slug' => 'our-brands','with_front' => false),
                'labels' => array(
                    'add_new_item'       => __( 'Add New Category', 'sage' ),
                    'parent_item_colon'  => __( 'Parent Category:', 'sage' ),
                )
            )
        );

        /**
         * Registers a Custom Post Type called Directory
         */
        register_post_type('directory', array(
            'labels' => array(
                'name'               => _x( 'Directories', 'post type general name', 'sage' ),
                'singular_name'      => _x( 'Directory', 'post type singular name', 'sage' ),
                'menu_name'          => _x( 'Directories', 'admin menu', 'sage' ),
                'name_admin_bar'     => _x( 'Directories', 'add new on admin bar', 'sage' ),
                'add_new'            => _x( 'Add New', 'Directories', 'sage' ),
                'add_new_item'       => __( 'Add New Directory', 'sage' ),
                'new_item'           => __( 'New Directory', 'sage' ),
                'edit_item'          => __( 'Edit Directory', 'sage' ),
                'view_item'          => __( 'View Directory', 'sage' ),
                'all_items'          => __( 'All Directories', 'sage' ),
                'search_items'       => __( 'Search Directories', 'sage' ),
                'parent_item_colon'  => __( 'Parent Directory:', 'sage' ),
                'not_found'          => __( 'No Directories item found.', 'sage' ),
                'not_found_in_trash' => __( 'No Directories item found in Trash.', 'sage' ),
            ),


            // Frontend
            'has_archive'        => true,
            'public'             => true,
            'publicly_queryable' => false,
            'hierarchical' => true,

            // Admin
            'capability_type' => 'post',
            'menu_icon'     => 'dashicons-location-alt',
            'menu_position' => 5,
            'query_var'     => true,
            'show_in_menu'  => true,
            'show_ui'       => true,
            'supports' => array( 'title', 'editor', 'thumbnail'),
        ));
        register_taxonomy(
            'directory-category',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'directory',        //post type name
            array(
                'hierarchical' => true, // true = category like, false = tag like
                'label' => __( 'Directory Categories' ),
                'show_admin_column' => true, // display in admin column
                'public' => false, // false = make private
                'show_in_menu'  => true,
                'show_ui'       => true,
                //'rewrite'       => array('slug' => 'our-brands','with_front' => false),
                'labels' => array(
                    'add_new_item'       => __( 'Add New Category', 'sage' ),
                    'parent_item_colon'  => __( 'Parent Category:', 'sage' ),
                )
            )
        );

    }
}

$themeACM = new themeACM;