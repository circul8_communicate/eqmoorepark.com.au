<?php use Roots\Sage\Titles; ?>

<?php $term = get_queried_object(); ?>
<?php $featured_image = get_field('featured_image', $term); ?>
<div class="page-header <?php echo ($featured_image) ? 'has-post-thumbnail' : ''; ?>">
    <?php if ( $featured_image ): ?>
        <figure class="bg-image bg-image_cover" style="background-image: url(<?php echo $featured_image['sizes']['large']; ?>);"></figure>
        <div class="page-title-wrap d-flex align-items-end">
            <div class="container">
                <h1 class="page-title"><?= Titles\title(); ?></h1>
            </div>
        </div>
    <?php else: ?>
        <div class="container">
            <h1 class="page-title"><?= Titles\title(); ?></h1>
        </div>
    <?php endif; ?>
</div>

<div class="page-content">
    <div class="container">
        <?php if(term_description()): ?>
            <div class="my-5">
                <?php echo term_description() ?>
            </div>
        <?php endif; ?>
        <?php if (!have_posts()) : ?>
            <div class="alert alert-warning">
                <?php _e('Sorry, no results were found.', 'sage'); ?>
            </div>
            <?php get_search_form(); ?>
        <?php endif; ?>
    </div>
</div>

<?php if(have_posts()) : ?>
    <div id="jsCardsEntry" class="cards-grid bg-navy">
        <div class="white-underlay"></div>
        <div class="container">
            <div class="row pb-4">
                <?php if($term->taxonomy === 'venue-category'): ?>
                    <?php query_posts($query_string . '&post_type=venue&orderby=title&order=asc&posts_per_page=-1'); ?>

                    <?php while (have_posts()) : the_post(); ?>
                        <?php if(get_field('display_position_prioritize_display')): ?>
                            <div class="col-lg-4 d-flex flex-column">
                                <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>

                    <?php while (have_posts()) : the_post(); ?>
                        <?php if(!get_field('display_position_prioritize_display')): ?>
                            <div class="col-lg-4 d-flex flex-column">
                                <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                            </div>
                        <?php endif; ?>
                    <?php endwhile; ?>

                <?php else: ?>

                    <?php while (have_posts()) : the_post(); ?>
                        <?php query_posts($query_string . '&posts_per_page=-1'); ?>
                        <div class="col-lg-4 d-flex flex-column">
                            <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if($term->taxonomy === 'venue-category'): ?>
    <?php get_template_part('templates/section', 'venue-hire'); ?>
<?php endif; ?>
