<article <?php post_class('entry-card flex-fill d-flex flex-column'); ?>>
    <div class="entry-card-thumbnail">
        <?php if ( has_post_thumbnail() ): ?>
            <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'large'); ?>)"></figure>
        <?php endif; ?>
    </div>

    <div class="entry-card-content">
        <header class="entry-card-header">
            <h2 class="entry-card-title mb-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        </header>
        <div class="entry-card-summary">
            <?php if($venue_information = get_field('venue_information')): ?>
                <div class="row">
                    <table class="table table-borderless">
                        <?php if($venue_information['venue_details']['venue_capacity']): ?>
                            <tr>
                                <td class="py-1 px-3">Capacity</td>
                                <td class="py-1 px-3"><?php echo $venue_information['venue_details']['venue_capacity']; ?></td>
                            </tr>
                        <?php endif; ?>
                        <?php if($venue_information['venue_details']['venue_floor_area']): ?>
                            <tr>
                                <td class="py-1 px-3">Floor Area</td>
                                <td class="py-1 px-3"><?php echo $venue_information['venue_details']['venue_floor_area']; ?></td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
                <?php if($venue_description = $venue_information['venue_details']['venue_description']): ?>
                    <?php echo wp_trim_words( $venue_description, 20, '...' ); ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="entry-card-footer mt-auto">
        <a class="btn btn-gold btn-block" href="<?php the_permalink(); ?>">Read More</a>
    </div>
</article>
