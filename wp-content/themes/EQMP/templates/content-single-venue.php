<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class('single'); ?>>
        <?php if(has_post_thumbnail()): ?>
            <?php get_template_part('templates/page-header'); ?>
        <?php else: ?>
            <header class="single-header">
                <div class="container">
                    <h1 class="single-title"><?php the_title(); ?></h1>
                </div>
            </header>
        <?php endif; ?>
        <div class="single-content">

            <?php if($venue_information = get_field('venue_information')): ?>
                <section class="fadein-up">
                    <div class="container my-5">
                        <div class="row">
                            <div class="col-lg-5 mb-3 mb-lg-0 pr-lg-5">
                                <div class="row">
                                    <table class="table table-borderless">
                                        <?php if($venue_information['venue_details']['venue_floor_area']): ?>
                                            <tr>
                                                <td><strong>Floor Area</strong></td>
                                                <td><?php echo $venue_information['venue_details']['venue_floor_area']; ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if($venue_information['venue_details']['venue_capacity']): ?>
                                            <tr>
                                                <td><strong>Capacity</strong></td>
                                                <td><?php echo $venue_information['venue_details']['venue_capacity']; ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if($venue_information['venue_details']['event_types']): ?>
                                            <tr>
                                                <td><strong>Event Types</strong></td>
                                                <td><?php echo $venue_information['venue_details']['event_types']; ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    </table>
                                </div>

                                <?php if($venue_information['venue_details']['venue_description']): ?>
                                    <div><?php echo $venue_information['venue_details']['venue_description']; ?></div>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-7 mb-3 mb-lg-0">
                                <?php if($venue_image_gallery = $venue_information['venue_image_gallery']): ?>
                                    <div class="image-gallery">
                                        <div class="image-gallery__images mb-2">
                                            <?php foreach($venue_image_gallery as $gallery_image): ?>
                                                <div class="image-item">
                                                    <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $gallery_image['sizes']['large']; ?>)"></figure>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="image-gallery__thumbnails">
                                            <?php foreach($venue_image_gallery as $gallery_image): ?>
                                                <div class="px-1 d-block">
                                                    <div class="thumbnail-item">
                                                        <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $gallery_image['sizes']['medium']; ?>)"></figure>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

            <?php get_template_part('templates/repeatable-sections'); ?>
        </div>
        <footer class="single-footer">
            <?php get_template_part('templates/section-form-content'); ?>
        </footer>
    </article>
<?php endwhile; ?>
