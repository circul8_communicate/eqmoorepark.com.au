<?php if($related_information = get_field('related_information')): ?>
    <?php $related_directory = $related_information['related_directory']; ?>
    <?php $related_events = $related_information['related_events']; ?>

    <?php if($related_directory || $related_events): ?>
      <section class="fadein-up">
          <div class="container">
              <hr class="sep sep-primary my-5">
              <h2 class="text-uppercase my-5">Related Information</h2>
          </div>
          <div id="jsCardsEntry" class="cards-grid bg-primary decor-top-most">
              <div class="white-underlay"></div>
              <div class="container">
                  <div class="row pb-4">
                      <?php if($related_directory): ?>
                          <?php $post = $related_directory; ?>
                          <div class="col-lg-4 d-flex flex-column">
                              <?php setup_postdata($post); ?>
                              <?php get_template_part('templates/content-directory'); ?>
                              <script src="<?php echo get_template_directory_uri(); ?>/assets/scripts/interactive-map.js"></script>
                          </div>
                          <?php wp_reset_postdata(); ?>
                      <?php endif; ?>

                      <?php if($related_events): ?>
                          <?php foreach($related_events as $post): ?>
                              <div class="col-lg-4 d-flex flex-column">
                                  <?php setup_postdata($post); ?>
                                  <?php get_template_part('templates/content-event'); ?>
                              </div>
                          <?php endforeach; ?>
                          <?php wp_reset_postdata(); ?>
                      <?php endif; ?>
                  </div>
              </div>
          </div>
      </section>
    <?php endif; ?>
<?php endif; ?>

<style>
  .cards-grid .btn-offer {
    background-color: #fff;
    color: #043868;
    border-color: #fff;
  }
  .cards-grid .btn-offer:hover,
  .cards-grid .btn-offer:focus {
    background-color: #043868;
    color: #fff;
    border-color: #043868;
  }
</style>
