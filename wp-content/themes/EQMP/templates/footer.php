<footer class="site-footer bg-dark text-white">
    <div class="container pt-5 pb-3">
        <div class="row mb-5">
            <div class="col col-lg-4">
                <div class="row">
                    <div class="col-lg-6">
                        <?php if (has_nav_menu('footer_links_1')) : ?>
                            <?php
                                wp_nav_menu( array(
                                    'theme_location'    => 'footer_links_1',
                                    'depth'             => 1,
                                    'container_class'   => '',
                                    'menu_class'        => 'nav flex-column',
                                ) );
                            ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6">
                        <?php if (has_nav_menu('footer_links_2')) : ?>
                            <?php
                                wp_nav_menu( array(
                                    'theme_location'    => 'footer_links_2',
                                    'depth'             => 1,
                                    'container_class'   => '',
                                    'menu_class'        => 'nav flex-column',
                                ) );
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col col-lg-4 ml-auto text-right">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-footer.svg" alt="<?php bloginfo('name'); ?>">
            </div>
        </div>

        <div class="row d-flex align-items-center">
            <div class="col-4 col-lg-4">
                <?php if($social_media_links = get_field('social_media_links', 'option')): ?>
                    <ul class="social list-inline">
                        <?php foreach($social_media_links as $social_link): ?>
                            <li class="list-inline-item"><a href="<?php echo $social_link['social_media_link']; ?>"><i class="fa <?php echo $social_link['social_media_icon']; ?>" aria-hidden="true"></i><span class="sr-only"><?php echo $social_link['social_media_name']; ?></span></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="col col-lg-4 ml-auto text-right">
                <?php if(get_field('copyright', 'option')): ?>
                    <p class="text-gold small"><span class="d-block d-lg-none">© 2018 EQ Moore Park</span><span class="d-none d-lg-block"></span><?php echo get_field('copyright', 'option'); ?></span></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>
