<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class('single'); ?>>
        <?php if(has_post_thumbnail()): ?>
            <div class="page-header has-post-thumbnail">
                <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full-screen'); ?>)"></figure>
            </div>
              <div class="container">
                  <h1 class="single-title text-dark text-uppercase my-5"><?php the_title(); ?></h1>
              </div>
        <?php else: ?>
            <header class="single-header">
                <div class="container">
                    <h1 class="single-title"><?php the_title(); ?></h1>
                </div>
            </header>
        <?php endif; ?>
        <div class="single-content mt-0">

            <?php if($event_information = get_field('event_information')): ?>
                <section class="fadein-up">
                    <div class="container my-5">
                        <div class="row">
                            <div class="col-lg-5 mb-3 mb-lg-0 pr-lg-5">
                                <div class="row">
                                    <table class="table table-borderless">
                                        <?php if($event_information['event_details']['event_date']): ?>
                                            <?php $start_date = $event_information['event_details']['event_date']['start_date']; ?>
                                            <?php $end_date = $event_information['event_details']['event_date']['end_date']; ?>
                                            <?php $formated_start_date = ($start_date) ? date('j M Y', strtotime($start_date)) : ''; ?>
                                            <?php $formated_end_date = ($end_date) ? date('j M Y', strtotime($end_date)) : ''; ?>

                                            <?php if($formated_start_date && $formated_end_date): ?>
                                                <tr>
                                                    <td><strong>Date</strong></td>
                                                    <td><?php echo $formated_start_date; ?> <?php echo ($formated_end_date) ? ' - ' . $formated_end_date : ''; ?></td>
                                                </tr>
                                            <?php elseif($formated_start_date && !$formated_end_date): ?>
                                                <tr>
                                                    <td><strong>Date</strong></td>
                                                    <td><?php echo $formated_start_date; ?></td>
                                                </tr>
                                            <?php elseif(!$formated_start_date && $formated_end_date): ?>
                                                <tr>
                                                    <td><strong>Date</strong></td>
                                                    <td>ENDS: <?php echo $formated_end_date; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php if($event_information['event_details']['event_date']['event_time']): ?>
                                            <tr>
                                                <td><strong>Time</strong></td>
                                                <td><?php echo $event_information['event_details']['event_date']['event_time']; ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if($event_information['event_details']['event_venue']): ?>
                                            <tr>
                                                <td><strong>Venue</strong></td>
                                                <td><?php echo $event_information['event_details']['event_venue']; ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if($event_information['event_details']['event_price']): ?>
                                            <tr>
                                                <td><strong>Tickets</strong></td>
                                                <td><?php echo $event_information['event_details']['event_price']; ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    </table>
                                </div>

                                <?php if($book_now = $event_information['event_details']['book_now']): ?>
                                    <a href="<?php echo $book_now['url']; ?>" class="btn btn-dark btn-wide" target="<?php echo $book_now['target']; ?>"><?php echo ($book_now['title']) ? $book_now['title'] : 'Book Now'; ?></a>
                                <?php endif; ?>
                                <?php if($more_info = $event_information['event_details']['more_info']): ?>
                                    <a href="<?php echo $more_info['url']; ?>" class="btn btn-dark btn-wide" target="<?php echo $more_info['target']; ?>"><?php echo ($more_info['title']) ? $more_info['title'] : 'More Info'; ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-7 mb-3 mb-lg-0">
                                <?php if($event_information['event_description']): ?>
                                    <?php echo $event_information['event_description']; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

            <?php get_template_part('templates/repeatable-sections'); ?>
        </div>
        <footer class="single-footer">
            <?php get_template_part('templates/section-events-related-info'); ?>
        </footer>
    </article>
<?php endwhile; ?>
