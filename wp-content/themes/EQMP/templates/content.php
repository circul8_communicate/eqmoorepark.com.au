<article <?php post_class('entry'); ?>>
    <header class="entry-header">
        <?php get_template_part('templates/entry-meta'); ?>
        <h2 class="entry-title mb-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php get_template_part('templates/entry-category'); ?>
    </header>

    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>

    <div class="entry-footer">
        <a class="btn btn-link" href="<?php the_permalink(); ?>">Read More</a>
    </div>
</article>
