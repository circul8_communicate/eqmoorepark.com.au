<?php if($venue_hire_section = get_field('venue_hire_section', 'option')): ?>
    <section class="bg-gold">
        <div class="container py-5 fadein-up">
            <div class="row">
                <div class="col-lg-4 mb-3 mb-lg-0">
                    <?php if($venue_hire_section['section_content']['section_title']): ?>
                        <h2 class="mb-4 text-uppercase"><?php echo $venue_hire_section['section_content']['section_title']; ?></h2>
                    <?php endif; ?>

                    <?php if($venue_hire_section['section_content']['section_copy']): ?>
                        <div><?php echo $venue_hire_section['section_content']['section_copy']; ?></div>
                    <?php endif; ?>
                </div>
                <div class="col-lg-8">
                    <?php $queried_object = get_queried_object(); ?>
                    <?php $venue_categories = get_terms( array( 'taxonomy' => 'venue-category') ); ?>
                    <div class="row">
                        <?php foreach($venue_categories as $venue_category): ?>
                            <?php if($queried_object->term_id !== $venue_category->term_id): ?>
                                <div class="col-lg-6 mb-3">
                                    <a href="<?php echo get_term_link($venue_category->term_id); ?>" class="btn btn-light text-center btn-block hover-primary"><?php echo $venue_category->name; ?></a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
