<?php if( have_rows('repeatable_sections') ): ?>
    <?php while ( have_rows('repeatable_sections') ) : the_row(); ?>

        <?php if( get_row_layout() == 'one_column_copy' ): ?>

            <!-- ONE COLUMN COPY -->
            <section class="fadein-up">
                <div class="container my-5">
                    <?php if($section_content = get_sub_field('section_content')): ?>
                        <?php echo $section_content['section_copy']; ?>
                    <?php endif; ?>
                </div>
            </section>
            <!-- END: ONE COLUMN COPY -->

        <?php elseif( get_row_layout() == 'two_column_copy' ): ?>

            <!-- TWO COLUMN COPY -->
            <section class="fadein-up">
                <div class="container my-5">
                    <div class="row">
                        <div class="col-lg-6 mb-3 mb-lg-0">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <?php echo $left_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-6 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <?php echo $right_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END: TWO COLUMN COPY -->

        <?php elseif( get_row_layout() == 'three_column_copy' ): ?>

            <!-- THREE COLUMN COPY -->
            <section class="fadein-up">
                <div class="container my-5">
                    <div class="row">
                        <div class="col-lg-4 mb-3 mb-lg-0">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <?php echo $left_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-4 mb-3 mb-lg-0">
                            <?php if($mid_content = get_sub_field('mid_content')): ?>
                                <?php echo $mid_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-4 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <?php echo $right_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END: THREE COLUMN COPY -->

        <?php elseif( get_row_layout() == 'four_column_copy' ): ?>

            <!-- FOUR COLUMN COPY -->
            <section class="fadein-up">
                <div class="container my-5">
                    <div class="row">
                        <div class="col-lg-3 mb-3 mb-lg-0">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <?php echo $left_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-3 mb-3 mb-lg-0">
                            <?php if($mid_left_content = get_sub_field('mid_left_content')): ?>
                                <?php echo $mid_left_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-3 mb-3 mb-lg-0">
                            <?php if($mid_right_content = get_sub_field('mid_right_content')): ?>
                                <?php echo $mid_right_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-3 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <?php echo $right_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END: TWO COLUMN COPY -->

        <?php elseif( get_row_layout() == 'one_column_image' ): ?>

            <!-- TWO COLUMN IMAGE -->
            <section class="fadein-up">
                <div class="container my-4">
                    <?php if($section_content = get_sub_field('section_content')): ?>
                        <div class="section-image">
                            <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $section_content['section_image']['sizes']['large']; ?>)"></figure>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
            <!-- END: TWO COLUMN IMAGE -->

        <?php elseif( get_row_layout() == 'two_column_image' ): ?>

            <!-- TWO COLUMN IMAGE -->
            <section class="fadein-up">
                <div class="container my-4">
                    <div class="row">
                        <div class="col-lg-6 mb-3 mb-lg-0">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <div class="section-image">
                                    <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $left_content['section_image']['sizes']['large']; ?>)"></figure>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-6 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <div class="section-image">
                                    <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $right_content['section_image']['sizes']['large']; ?>)"></figure>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END: TWO COLUMN IMAGE -->

        <?php elseif( get_row_layout() == 'three_column_image' ): ?>

            <!-- THREE COLUMN IMAGE -->
            <section class="fadein-up">
                <div class="container my-4">
                    <div class="row">
                        <div class="col-lg-4 mb-3 mb-lg-0">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <div class="section-image">
                                    <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $left_content['section_image']['sizes']['large']; ?>)"></figure>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-4 mb-3 mb-lg-0">
                            <?php if($mid_content = get_sub_field('mid_content')): ?>
                                <div class="section-image">
                                    <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $mid_content['section_image']['sizes']['large']; ?>)"></figure>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-4 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <div class="section-image">
                                    <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $right_content['section_image']['sizes']['large']; ?>)"></figure>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END: THREE COLUMN IMAGE -->

        <?php elseif( get_row_layout() == 'video_with_left_copy' ): ?>

            <!-- VIDEO WITH COPY LEFT -->
            <section class="fadein-up">
                <div class="container my-5">
                    <div class="row">
                        <div class="col-lg-5 mb-3 mb-lg-0 pr-lg-5">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <?php echo $left_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-7 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <div class="embed-responsive embed-responsive-16by9 bg-dark">
                                    <?php echo $right_content['section_video']; ?>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </section>
            <!-- END: VIDEO WITH COPY LEFT -->

        <?php elseif( get_row_layout() == 'map_with_left_copy' ): ?>

            <!-- MAP WITH COPY LEFT -->
            <section class="fadein-up">
                <div class="container my-5">
                    <div class="row">
                        <div class="col-lg-5 mb-3 mb-lg-0 pr-lg-5">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <?php echo $left_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-7 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <div class="embed-responsive embed-responsive-16by9 bg-dark">
                                    <?php echo $right_content['section_map']; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END: MAP WITH COPY LEFT -->

        <?php elseif( get_row_layout() == 'image_with_left_copy' ): ?>

            <!-- IMAGE WITH LEFT COPY -->
            <section class="fadein-up">
                <div class="container my-5">
                    <div class="row">
                        <div class="col-lg-5 mb-3 mb-lg-0 pr-lg-5">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <?php echo $left_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-7 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <div class="section-image">
                                    <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $right_content['section_image']['sizes']['large']; ?>)"></figure>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END: IMAGE WITH LEFT COPY -->

        <?php elseif( get_row_layout() == 'gallery_with_left_copy' ): ?>

            <!-- GALLERY WITH LEFT COPY -->
            <section class="fadein-up">
                <div class="container my-5">
                    <div class="row">
                        <div class="col-lg-5 mb-3 mb-lg-0 pr-lg-5">
                            <?php if($left_content = get_sub_field('left_content')): ?>
                                <?php echo $left_content['section_copy']; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-7 mb-3 mb-lg-0">
                            <?php if($right_content = get_sub_field('right_content')): ?>
                                <div class="image-gallery">
                                    <?php if($section_gallery = $right_content['section_gallery']): ?>
                                        <div class="image-gallery__images mb-2">
                                            <?php foreach($section_gallery as $gallery_image): ?>
                                                <div class="image-item">
                                                    <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $gallery_image['sizes']['large']; ?>)"></figure>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($section_gallery = $right_content['section_gallery']): ?>
                                        <div class="image-gallery__thumbnails">
                                            <?php foreach($section_gallery as $gallery_image): ?>
                                                <div class="px-1 d-block">
                                                    <div class="thumbnail-item">
                                                        <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $gallery_image['sizes']['medium']; ?>)"></figure>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END: GALLERY WITH LEFT COPY -->

        <?php endif; ?>

   <?php  endwhile; ?>
<?php endif; ?>
