<?php get_template_part('templates/page', 'header'); ?>

<div class="page-content">
    <div class="container">
        <?php if (!have_posts()) : ?>
          <div class="alert alert-warning">
            <?php _e('Sorry, no results were found.', 'sage'); ?>
          </div>
          <?php get_search_form(); ?>
        <?php endif; ?>

        <?php if(have_posts()) : ?>
        <div id="jsCardsEntry" class="cards-grid bg-navy">
            <div class="white-underlay"></div>
            <div class="container">
                <div class="row pb-4">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-lg-4 d-flex flex-column">
                            <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>

        <?php the_posts_navigation(); ?>
    </div>
</div>
