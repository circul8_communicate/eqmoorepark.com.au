<article <?php post_class('entry-card flex-fill d-flex flex-column'); ?>>
    <div class="entry-card-thumbnail">
        <?php if ( has_post_thumbnail() ): ?>
            <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'large'); ?>)"></figure>
        <?php endif; ?>
    </div>

    <?php if($event_information = get_field('event_information')): ?>
        <div class="entry-card-content">
            <header class="entry-card-header d-flex">
                <div>
                    <?php $start_date = get_field('event_information_event_details_event_date_start_date', false, false); ?>
                        <?php if($start_date): ?>
                            <?php $event_start_date = new DateTime($start_date); ?>
                            <?php $event_start_month = $event_start_date->format('M'); ?>
                        <?php endif; ?>
                    <?php $end_date = get_field('event_information_event_details_event_date_end_date', false, false); ?>
                        <?php if($end_date): ?>
                            <?php $event_end_date = new DateTime($end_date); ?>
                            <?php $event_end_month = $event_end_date->format('M'); ?>
                        <?php endif; ?>

                    <?php if(($start_date && $end_date) && ($start_date !== $end_date )): ?>
                        <div class="entry-card-date">
                            <div class="entry-card-date__month text-center"><?php echo $event_start_month; ?> <?php echo ($event_end_month && ($event_start_month !== $event_end_month)) ? '- ' . $event_end_month : ''; ?></div>
                            <div class="entry-card-date__days text-center"><?php echo $event_start_date->format('j'); ?><?php echo ($event_end_date) ? ' - ' . $event_end_date->format('j') : ''; ?></div>
                        </div>
                    <?php elseif($start_date && !$end_date): ?>
                        <div class="entry-card-date">
                            <div class="entry-card-date__month text-center"><?php echo $event_start_month; ?></div>
                            <div class="entry-card-date__days text-center"><?php echo $event_start_date->format('j'); ?></div>
                        </div>
                    <?php elseif(!$start_date && $end_date): ?>
                        <div class="entry-card-date">
                            <div class="entry-card-date__month text-center">Ends <?php echo $event_end_month; ?></div>
                            <div class="entry-card-date__days text-center"><?php echo $event_end_date->format('j'); ?></div>
                        </div>
                    <?php endif; ?>
                </div>
                <div>
                    <h2 class="entry-card-title mb-1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php if($event_information['event_details']['event_venue']): ?>
                        <h3 class="entry-card-subtitle mb-3"><?php echo $event_information['event_details']['event_venue']; ?></h3>
                    <?php endif; ?>
                </div>
            </header>
            <div class="entry-card-summary">
                <?php if($event_description = $event_information['event_description']): ?>
                    <?php echo wp_trim_words( $event_description, 20, '...' ); ?>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="entry-card-footer mt-auto">
        <a class="btn btn-gold btn-block" href="<?php the_permalink(); ?>">Read More</a>
    </div>
</article>
