<?php if($category =  get_the_terms(get_the_ID(), 'category')): ?>
    <?php $term =  array_pop($category); ?>
    <div class="entry-category">
        <a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a>
    </div>
<?php endif; ?>
