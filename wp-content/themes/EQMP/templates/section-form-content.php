<?php if($form_shortcode = get_field('form_section_form_shortcode')): ?>
<section class="fadein-up">
    <div class="container my-5">
        <div class="form-container">
            <?php echo do_shortcode($form_shortcode); ?>
        </div>
    </div>
</section>
<?php endif; ?>
