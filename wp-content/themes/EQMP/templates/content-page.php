<div class="page-content">
    <div class="container fadein-up">
        <?php the_content(); ?>
    </div>
    <?php get_template_part('templates/repeatable-sections'); ?>
    <?php get_template_part('templates/section-form-content'); ?>
</div>
