<article <?php post_class('entry-card-alt flex-fill d-flex flex-column'); ?>>
    <div class="entry-card-thumbnail">
        <?php if ( has_post_thumbnail() ): ?>
            <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'large'); ?>)"></figure>
        <?php endif; ?>
    </div>

    <div class="entry-card-content">
        <header class="entry-card-header">
            <h2 class="entry-card-title mb-3"><?php the_title(); ?></h2>
        </header>

        <?php if($directory_information = get_field('directory_information')): ?>
            <div class="entry-card-summary">
                <div class="row">
                    <?php if($opening_hours = $directory_information['directory_details']['opening_hours']): ?>
                        <table class="table table-borderless mb-3">
                            <?php foreach($opening_hours as $opening_hour): ?>
                                <tr>
                                    <td class="py-1 px-3"><?php echo $opening_hour['day']; ?></td>
                                    <td class="py-1 px-3"><?php echo $opening_hour['time']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                </div>
                <?php if($directory_information['directory_details']['contact_number']): ?>
                    <p><i class="fa fa-phone" aria-hidden="true" style="font-size: 20px;"></i> &nbsp; <?php echo $directory_information['directory_details']['contact_number']; ?></p>
                <?php endif; ?>
                <?php if($directory_information['directory_details']['interactive_map_id']): ?>
                    <p><i class="fa fa-map-marker" aria-hidden="true"  style="font-size: 20px;"></i>
                    &nbsp;<a href="#" class="view-directory-map" data-detail-title="<?php the_title(); ?>" data-detail-id="<?php echo $directory_information['directory_details']['interactive_map_id']; ?>">Find our location</a></p>
                <?php else: ?>
                  <?php if($directory_information['directory_details']['building_number']): ?>
                      <p><i class="fa fa-building" aria-hidden="true"  style="font-size: 20px;"></i> &nbsp; <?php echo $directory_information['directory_details']['building_number']; ?></p>
                  <?php endif; ?>
                <?php endif; ?>
                <?php if($directory_information['directory_details']['interactive_map_id']): ?>
                    <p><i class="fa fa-map-marker" aria-hidden="true"  style="font-size: 20px;"></i> &nbsp; <a href="/maps-staging/#<?php echo $directory_information['directory_details']['interactive_map_id']; ?>">Find our location</a></p>
                <?php endif; ?>

                <?php if($directory_description = $directory_information['directory_description']): ?>
                    <div><?php echo wp_trim_words( $directory_description, 12, '...' ); ?></div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="entry-card-footer mt-auto">
        <div class="row">
        <?php if($directory_information['learn_more_link']): ?>
            <div class="col">
                <a class="btn btn-dark btn-block hover-primary" href="<?php echo $directory_information['learn_more_link']['url']; ?>" style="min-width: 0;"><?php echo ($directory_information['learn_more_link']['title']) ? $directory_information['learn_more_link']['title'] : 'Learn More'; ?></a>
            </div>
        <?php endif; ?>

        <?php if($directory_information['offer_message_info']['offer_description']): ?>
            <div class="col">
                <button type="button" class="btn btn-primary btn-block hover-navy btn-offer" data-toggle="modal" data-target="#offer<?php echo get_the_ID(); ?>" style="min-width: 0;">Offer message</button>
            </div>
            <div class="modal fade" id="offer<?php echo get_the_ID(); ?>"  tabindex="-1" role="dialog" aria-labelledby="<?php echo get_the_ID(); ?>Label" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title h6" id="offer<?php echo get_the_ID(); ?>Label"><?php echo $directory_information['offer_message_info']['offer_title']; ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body p-3">
                            <?php echo $directory_information['offer_message_info']['offer_description']; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        </div>
    </div>
</article>
