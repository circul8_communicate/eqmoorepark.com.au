<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class('single'); ?>>
        <?php if(has_post_thumbnail()): ?>
            <?php get_template_part('templates/page-header'); ?>
        <?php else: ?>
            <header class="single-header">
                <div class="container">
                    <h1 class="single-title"><?php the_title(); ?></h1>
                </div>
            </header>
        <?php endif; ?>
        <div class="single-content">
            <div class="container">
                <?php the_content(); ?>
            </div>

            <?php get_template_part('templates/repeatable-sections'); ?>
        </div>
        <footer class="single-footer">
            <div class="container">
                <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
            </div>
        </footer>
    </article>
<?php endwhile; ?>
