<header class="site-header bg-white">
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid px-0">
            <div class="navbar-bar d-flex align-items-center flex-fill">
                <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
                    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="<?php bloginfo('name'); ?>">
                </a>

                <?php if (has_nav_menu('primary_navigation')) : ?>
                <button id="jsNavbarToggler" class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#jsPrimaryMenu" aria-controls="jsPrimaryMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <?php endif; ?>
            </div>

            <div id="jsPrimaryMenu" class="collapse navbar-collapse">
                <div class="navbar-search d-block d-lg-none py-4 px-3">
                    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                        <button type="submit" class="search-submit"><?php echo esc_attr_x( 'Search', 'submit button' ) ?></button>
                        <label for="s" class="w-100 mb-0">
                            <span class="sr-only"><?php echo _x( 'Search for:', 'label' ) ?></span>
                            <input type="search" class="search-field w-100" value="<?php echo get_search_query() ?>" name="s" id="s"  required/>
                        </label>
                    </form>
                </div>
                <?php if (has_nav_menu('primary_navigation')) : ?>
                    <?php
                        wp_nav_menu( array(
                            'theme_location'    => 'primary_navigation',
                            'depth'             => 3,
                            'container_class'   => 'ml-auto px-3 pb-3 p-lg-0',
                            'menu_class'        => 'nav navbar-nav',
                            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                            'walker'            => new WP_Bootstrap_Navwalker(),
                        ) );
                    ?>
                <?php endif; ?>
            </div>

            <div class="navbar-search d-none d-xl-block ml-5">
                <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                    <button type="submit" class="search-submit"><?php echo esc_attr_x( 'Search', 'submit button' ) ?></button>
                    <label for="s" class="w-100 mb-0">
                        <span class="sr-only"><?php echo _x( 'Search for:', 'label' ) ?></span>
                        <input type="search" class="search-field w-100" value="<?php echo get_search_query() ?>" name="s" id="s"  required/>
                    </label>
                </form>
            </div>
        </div>

    </nav>
</header>
