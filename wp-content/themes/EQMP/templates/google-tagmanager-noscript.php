<?php if($google_tag_manager_id = get_field('google_tag_manager_id', 'option')): ?>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $google_tag_manager_id; ?>"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php endif; ?>
