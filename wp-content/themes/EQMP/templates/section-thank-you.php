<div class="container">
    <h1 class="text-uppercase mb-5">Thank you for your enquiry</h1>

    <div class="mb-5">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed nisi vehicula, facilisis mi non, faucibus mauris. Nullam euismod iaculis elit, nec eleifend eros sodales aliquam. Nam bibendum, libero sed consequat interdum, ex erat dapibus elit.</p>
    </div>

    <a href="#" class="btn btn-dark btn-wide">Return home</a>
</div>
