<?php if($instagram_feed_section = get_field('instagram_feed_section', 'option')): ?>
    <section class="bg-dark">
        <div class="container py-5">
            <?php if($instagram_feed_section['instagram_feed_shortcode']): ?>
                <?php echo do_shortcode($instagram_feed_section['instagram_feed_shortcode']); ?>
            <?php else: ?>
                <div class="text-center text-warning">No Instagram feed added.</div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>