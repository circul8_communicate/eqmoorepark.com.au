<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class('single'); ?>>
        <?php if(has_post_thumbnail()): ?>
            <?php get_template_part('templates/page-header'); ?>
        <?php else: ?>
            <header class="single-header">
                <div class="container">
                    <h1 class="single-title"><?php the_title(); ?></h1>
                </div>
            </header>
        <?php endif; ?>
        <div class="single-content">

            <?php if($directory_information = get_field('directory_information')): ?>
                <section class="fadein-up">
                    <div class="container my-5">
                        <div class="row">
                            <div class="col-lg-5 mb-3 mb-lg-0 pr-lg-5">
                                <?php if($directory_information['directory_details']['contact_number']): ?>
                                    <p><i class="fa fa-phone" aria-hidden="true" style="font-size: 20px;"></i> &nbsp; <?php echo $directory_information['directory_details']['contact_number']; ?></p>
                                <?php endif; ?>
                                <?php if($directory_information['directory_details']['building_number']): ?>
                                    <p><i class="fa fa-building" aria-hidden="true"  style="font-size: 20px;"></i> &nbsp; <?php echo $directory_information['directory_details']['building_number']; ?></p>
                                <?php endif; ?>
                                <?php if($directory_information['directory_details']['website_link']): ?>
                                    <p><i class="fa fa-globe" aria-hidden="true" style="font-size: 20px;"></i> &nbsp; <a class="text-dark" href="<?php echo $directory_information['directory_details']['website_link']['url']; ?>" target="_blank"><?php echo $directory_information['directory_details']['website_link']['title']; ?></a></p>
                                <?php endif; ?>

                                <?php if($opening_hours = $directory_information['directory_details']['opening_hours']): ?>
                                    <h3 class="h6 mt-4">Opening Hours</h3>
                                    <table class="table table-borderless mb-3">
                                        <?php foreach($opening_hours as $opening_hour): ?>
                                            <tr>
                                                <td class="py-1 px-3"><?php echo $opening_hour['day']; ?></td>
                                                <td class="py-1 px-3"><?php echo $opening_hour['time']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                <?php endif; ?>



                            </div>
                            <div class="col-lg-7 mb-3 mb-lg-0">
                                <?php if($directory_information['directory_description']): ?>
                                    <?php echo $directory_information['directory_description']; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
        </div>
    </article>
<?php endwhile; ?>
