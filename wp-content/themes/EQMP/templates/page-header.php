<?php use Roots\Sage\Titles; ?>


<?php if (is_home() && get_option('page_for_posts') ): ?>
    <div class="page-header has-post-thumbnail">
        <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'full-screen'); ?>
        <?php $featured_image = $img[0]; ?>
        <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $featured_image; ?>)"></figure>
        <div class="page-title-wrap d-flex align-items-end">
            <div class="container">
                <h1 class="page-title"><?= Titles\title(); ?></h1>
            </div>
        </div>
    </div>
<?php elseif( is_single()): ?>
    <div class="page-header has-post-thumbnail">
        <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full-screen'); ?>)"></figure>
    </div>
    <div class="container">
        <h1 class="page-title page-title text-dark py-0 my-5" style="transform: translateY(1rem);"><?= Titles\title(); ?></h1>
    </div>
<?php elseif ( has_post_thumbnail() && !is_404()): ?>
    <div class="page-header has-post-thumbnail">
        <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full-screen'); ?>)"></figure>
        <div class="page-title-wrap d-flex align-items-end">
            <div class="container">
                <h1 class="page-title"><?= Titles\title(); ?></h1>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="page-header">
        <div class="container">
            <h1 class="page-title"><?= Titles\title(); ?></h1>
        </div>
    </div>
<?php endif; ?>
