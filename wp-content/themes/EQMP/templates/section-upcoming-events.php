<?php if($upcoming_events_section = get_field('upcoming_events_section', 'option')): ?>
    <section class="fadein-up">
        <?php $upcoming_events = $upcoming_events_section['upcoming_events']; ?>

        <?php if($upcoming_events): ?>
            <div class="container">
                <hr class="sep sep-primary my-5">
                <h2 class="text-uppercase my-5">Upcoming Events</h2>
            </div>
            <div id="jsCardsEntry" class="cards-grid bg-primary decor-top-most">
                <div class="white-underlay"></div>
                <div class="container">
                    <div class="row pb-4">

                        <?php foreach($upcoming_events as $post): ?>
                            <?php setup_postdata($post); ?>
                            <div class="col-lg-4 d-flex flex-column">
                                <?php get_template_part('templates/content-event'); ?>
                            </div>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <?php if($upcoming_events_section['load_more']): ?>
                        <div class="text-center mb-5 fadein-up">
                            <a href="<?php echo $upcoming_events_section['load_more']; ?>" class="btn btn-link btn-link_center text-uppercase text-white">Load More</a>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if(($venue_hire_section = get_field('venue_hire_section')) && ($venue_hire_section['display_section'] === 'yes')): ?>
                    <?php get_template_part('templates/section', 'venue-hire'); ?>
                <?php endif; ?>

            </div>
        <?php endif; ?>

    </section>
<?php endif; ?>


