<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
    <?php get_template_part('templates/google-tagmanager'); ?>
    <style>
       .btn-wide {
            min-width: 0;
            max-width: 23.75rem;
            width: 100%;
       }
    </style>
</head>