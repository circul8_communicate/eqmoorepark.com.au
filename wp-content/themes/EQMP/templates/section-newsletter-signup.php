<?php if($newsletter_signup_section = get_field('newsletter_signup_section', 'option')): ?>
    <div class="bg-dark my-1">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-lg-7 mb-3 mb-lg-0">
                    <h2 class="h4 mb-0 text-uppercase text-center text-white"><?php echo $newsletter_signup_section['newsletter_signup']['label']; ?></h2>
                </div>
                <div class="col-lg-5 text-center">
                    <button class="btn btn-gold hover-primary" data-toggle="modal" data-target="#newsletterSignup">Subscribe</button>

                    <div class="modal fade" id="newsletterSignup" tabindex="-1" role="dialog" aria-labelledby="newsletterSignupLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header d-block">
                                    <a class="navbar-brand mb-4" href="<?= esc_url(home_url('/')); ?>">
                                        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="<?php bloginfo('name'); ?>">
                                    </a>
                                    <h5 class="modal-title text-center text-uppercase mb-4" id="newsletterSignupTitle">Subscribe to EQ <br>updates</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body text-left">
                                    <?php echo do_shortcode($newsletter_signup_section['newsletter_signup']['form_shortcode']); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
