<?php get_template_part('templates/page', 'header'); ?>

<div class="page-content my-0">

  <?php if (!have_posts()) : ?>
  <div class="container py-5">
    <div class="alert alert-warning">
      <?php _e('Sorry, no results were found.', 'sage'); ?>
    </div>
    <?php get_search_form(); ?>
  </div>
  <?php endif; ?>

  <?php /**
		<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'search'); ?>
  <?php endwhile; ?>
  **/ ?>

  <?php if( have_posts() ): ?>
  <?php $types = array('event', 'directory', 'venue','page', 'post'); ?>
  <?php foreach( $types as $type ): ?>

  <?php if($type === 'post' && ('post' === get_post_type())): ?>
  <div class="container py-5">
    <h3 class="mb-4">NEWS</h3>
    <?php while( have_posts() ): the_post(); ?>
    <?php if( $type == get_post_type() ): ?>
    <?php get_template_part('templates/content', 'search'); ?>
    <?php endif; ?>
    <?php endwhile; ?>
  </div>
  <?php endif; ?>

  <?php if($type === 'page' && ('page' === get_post_type())): ?>
  <div class="container py-5">
    <h3 class="mb-4">PAGES</h3>
    <?php while( have_posts() ): the_post(); ?>
    <?php if( $type == get_post_type() ): ?>
    <?php get_template_part('templates/content', 'search'); ?>
    <?php endif; ?>
    <?php endwhile; ?>
  </div>
  <?php endif; ?>

  <?php if($type === 'event' && ('event' === get_post_type())): ?>
    <div class="cards-grid bg-primary">
      <div class="container py-5">
        <h3 class="mb-4 text-white">EVENTS</h3>
        <div class="row pb-4">
          <?php while( have_posts() ): the_post(); ?>
          <?php if( $type == get_post_type() ): ?>
          <div class="col-lg-4 d-flex flex-column">
            <?php get_template_part('templates/content-event'); ?>
          </div>
          <?php endif; ?>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if($type === 'directory' && ('directory' === get_post_type())): ?>
    <div class="cards-grid bg-gold">
      <div class="container py-5">
        <h3 class="mb-4 text-white">DIRECTORIES</h3>
        <div class="row pb-4">
          <?php while( have_posts() ): the_post(); ?>
          <?php if( $type == get_post_type() ): ?>
          <div class="col-lg-4">
            <?php get_template_part('templates/content-directory'); ?>
          </div>
          <?php endif; ?>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/scripts/interactive-map.js"></script>
  <?php endif; ?>

  <?php if($type === 'venue' && ('venue' === get_post_type())): ?>
    <div class="cards-grid bg-navy">
      <div class="container py-5">
        <h3 class="mb-4 text-white">VENUES</h3>
        <div class="row pb-4">
          <?php while( have_posts() ): the_post(); ?>
          <?php if( $type == get_post_type() ): ?>
          <div class="col-lg-4">
            <?php get_template_part('templates/content-venue'); ?>
          </div>
          <?php endif; ?>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php rewind_posts(); ?>
  <?php endforeach; ?>
  <?php endif; ?>

  <div class="container">
    <?php the_posts_navigation(); ?>
  </div>

</div>
