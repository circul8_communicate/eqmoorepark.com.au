<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip;';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');



//Add Custom Style on WP admin page
function admin_page_custom_style() {
  echo '<style>
    #menu-pages a.menu-top,
    #menu-posts a.menu-top,
    #menu-posts-event a.menu-top,
    #menu-posts-venue a.menu-top,
    #menu-posts-directory a.menu-top,
    #toplevel_page_wpcf7 a.menu-top,
    #toplevel_page_acf-options-general-settings a.menu-top {
        color: #f91d1d !important;
    }
  </style>';
}
add_action('admin_head', __NAMESPACE__ . '\\admin_page_custom_style');



/**
 * Options Page
 */
if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
    'page_title'  => 'Theme Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => true
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'General Settings',
    'menu_title'  => 'General Settings',
    'parent_slug' => 'theme-general-settings'
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Header Settings',
    'menu_title'  => 'Header Settings',
    'parent_slug' => 'theme-general-settings'
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Footer Settings',
    'menu_title'  => 'Footer Settings',
    'parent_slug' => 'theme-general-settings'
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Common Sections',
    'menu_title'  => 'Common Sections',
    'parent_slug' => 'theme-general-settings'
  ));

}

function change_wp_search_size($queryVars) {
    if ( isset($_REQUEST['s']) ) // Make sure it is a search page
        $queryVars['posts_per_page'] = -1; // Change 10 to the number of posts you would like to show
    return $queryVars; // Return our modified query variables
}
add_filter('request', __NAMESPACE__ . '\\change_wp_search_size'); // Hook our custom function onto the request filter
