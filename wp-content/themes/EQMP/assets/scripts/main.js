/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        // PRELOADER
        // $(window).on('load', function() {
        $(document).ready(function () {
            $('.loader').fadeOut();
            $('.loader-mask').delay(350).fadeOut('slow');
        });

        // NAVBAR MENU COLLAPSE
        (function () {
            $('#jsPrimaryMenu').on('show.bs.collapse', function() {
                $('body').addClass('nav-is-visible');
            });
            $('#jsPrimaryMenu').on('hide.bs.collapse', function() {
                $('body').removeClass('nav-is-visible');
            });
        })();

        // NAVBAR DROPDOWN MENU
        (function () {
            $('#jsPrimaryMenu').on('hover', '.dropdown', function() {
                $(this).children('.dropdown-menu').toggleClass('show');
            });

            var $dropdown_toggle = $('#jsPrimaryMenu a.dropdown-toggle');

            enquire.register("screen and (min-width: 992px)", {
                match : function() {
                    $dropdown_toggle.on('click', function() {
                        var $a = $(this);
                        if($a.siblings('.dropdown-menu').hasClass('show')) {
                          if ($a.length && $a.attr('href')) {
                              location.href = $a.attr('href');
                          }
                        }
                    });
                },
                unmatch : function() {
                    $dropdown_toggle.unbind();
                }
            });


        })();

        // SCROLL REVEAL
        (function () {
            var fadeInUp = {
                delay: 200,
                duration: 1500,
                interval: 200,
                distance: '100px',
                origin: 'bottom',
                easing: 'ease',
                viewOffset: { top: 60 }
            };
            var fadeInLeft = {
                delay: 500,
                interval: 200,
                distance: '100px',
                origin: 'right',
                viewOffset: { top: 60 }
            };
            var fadeInRight = {
                delay: 500,
                interval: 200,
                distance: '100px',
                origin: 'left',
                viewOffset: { top: 60 }
            };

            new ScrollReveal().reveal('.fadein-up', fadeInUp);
            new ScrollReveal().reveal('.fadein-left', fadeInLeft);
            new ScrollReveal().reveal('.fadein-right', fadeInRight);

            new ScrollReveal().reveal('#jsBlogPosts .entry', fadeInUp );
            new ScrollReveal().reveal('#jsCardsEntry .entry-card', fadeInUp );

        })();

        // CARDS GRID WHITE UNDERLAY
        (function () {
          function white_underlay_height() {
            var thumbnail_height = $('.cards-grid').first('.entry-card').find('.entry-card-thumbnail>.bg-image').height();
            $(".white-underlay").height(thumbnail_height);
          }

          $(document).ready(function() {
            white_underlay_height();
            $(window).resize(white_underlay_height);
          });

        })();

        // CONTACT FORM
        (function () {
          var $form = $('.wpcf7-form');
          var validationErrors = false;

          function isValidEmail(email) {
            var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
            return pattern.test(email);
          }

          function isValidNumber(phone) {
            var pattern = new RegExp(/^([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})/i);
            return pattern.test(phone);
          }

          if($form) {
            $form.find('input[type=text], input[type=email],input[type=tel], select, textarea')
            .each(function(index, el) {
              $(el).wrap('<div class="input-wrapper"></div>');
            });

            $form.on('blur', '.form-control, .custom-select', function(event) {
              var input = $(this);
              var parent = input.parent('.input-wrapper');
              var inputValue = input.val();
              var inputRequired = input.hasClass('wpcf7-validates-as-required');
              var inputType = input.attr('type');

              validationErrors = false;
              parent.removeClass('focused');

              if(inputRequired && inputValue === '') {
                validationErrors = true;
              } else if(inputType === 'email' && !isValidEmail(inputValue)) {
                validationErrors = true;
              } else if(inputType === 'tel' && !isValidNumber(inputValue)) {
                validationErrors = true;
              }

              if(validationErrors) {
                parent.removeClass('valid');
                parent.addClass('invalid');
              } else {
                parent.addClass('valid');
                parent.removeClass('invalid');
              }

            }).on('focus', '.form-control', function(event) {
              var input = $(this);
              var parent = input.parent('.input-wrapper');

              parent.addClass('focused');
              parent.removeClass('valid invalid');
            });
          }

        })();


        // IMAGE GALLERY
        (function () {
            var $image_gallery = $('.image-gallery');

            $image_gallery.each(function() {
              var $image_gallery_images = $(this).find('.image-gallery__images');
              var $image_gallery_thumbnails = $(this).find('.image-gallery__thumbnails');

              $image_gallery_images.slick({
                  dots: false,
                  arrows: false,
                  infinite: false,
                  speed: 500,
                  fade: false,
                  cssEase: 'linear',
                  asNavFor: $image_gallery_thumbnails
              });
              $image_gallery_thumbnails.slick({
                  slidesToShow: 5,
                  slidesToScroll: 1,
                  infinite: true,
                  lazyLoad: 'ondemand',
                  asNavFor: $image_gallery_images,
                  dots: false,
                  centerMode: false,
                  focusOnSelect: true
              });
            });
        })();

        // HERO SLIDER
        (function () {
            $("#jsHeroImagesSlider").slick({
                dots: false,
                arrows: false,
                infinite: true,
                speed: 500,
                autoplay: true,
                fade: true,
                cssEase: 'linear',
                asNavFor: '#jsHeroCopiesSlider'
            });
            $("#jsHeroCopiesSlider").slick({
                dots: true,
                arrows: false,
                infinite: true,
                speed: 500,
                autoplay: true,
                fade: true,
                cssEase: 'linear',
                asNavFor: '#jsHeroImagesSlider'
            });
        })();

        // LAZYLOAD IMAGES
        (function () {
          var lazyLoadInstance = new LazyLoad({
              elements_selector: ".lazy"
              // ... more custom settings?
          });
        })();

        // DIREECTORIES ISOTOPE
        (function () {
            var qsRegex;
            var buttonFilter;
            var selectFilter;
            var $container = $('#jsDirectories');

            // debounce so filtering doesn't happen every millisecond
            function debounce( fn, threshold ) {
              var timeout;
              threshold = threshold || 100;
              return function debounced() {
                clearTimeout( timeout );
                var args = arguments;
                var _this = this;
                function delayed() {
                  fn.apply( _this, args );
                }
                timeout = setTimeout( delayed, threshold );
              };
            }

            if($container.length) {
              $container.isotope({
                itemSelector: '.directory-item-wrap',
                filter: function() {
                  var $this = $(this);
                  var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
                  var buttonResult = buttonFilter ? $this.is( buttonFilter ) : true;
                  var selectResult = selectFilter ? $this.is( selectFilter ) : true;
                  return searchResult && buttonResult && selectResult;
                },
              });



              // use value of search field to filter
              var $quicksearch = $('.explore-eq-search .search-field').keyup( debounce( function() {
                qsRegex = new RegExp( $quicksearch.val(), 'gi' );
                $container.isotope();
              }, 200 ) );

              // bind filter button click
              $selectFilters = $('#jsSelectFilters select').on( 'change', function(e) {
                var filterValue = this.value;
                buttonFilter = '';
                selectFilter = filterValue;
                $container.isotope();
              });

              // bind filter button click
              $filters = $('#jsFilters').on( 'click', 'button', function(e) {
                e.preventDefault();

                var $this = $( this );
                var filterValue;
                if ( $this.is('.is-checked') ) {
                  // uncheck
                  filterValue = '*';
                } else {
                  filterValue = $this.attr('data-filter');
                  $filters.find('.is-checked').removeClass('is-checked');
                }
                $this.toggleClass('is-checked');

                // use filterFn if matches value
                filterValue = filterValue;
                selectFilter = '';
                buttonFilter = filterValue;
                $container.isotope();
              });
            }

        }());

        // SMOOTH HASH SCROLL
        (function () {
            // Select all links with hashes
            $('a[href*="#"]')
              // Remove links that don't actually link to anything
              .not('[href="#"]')
              .not('[href="#0"]')
              .click(function(event) {
                // On-page links
                if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                  // Figure out element to scroll to
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                  // Does a scroll target exist?
                  if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                      scrollTop: target.offset().top
                    }, 500, function() {
                      // Callback after animation
                      // Must change focus!
                      var $target = $(target);
                      $target.focus();
                      if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                      } else {
                        $target.attr('tabindex','-1').css('outline', '0'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                      }
                    });
                  }
                }
              });
        })();

        // INITIALIZE HEADROOM
        (function() {
            // var header = document.querySelector(".className");
            // if(window.location.hash) {
            //   header.classList.add("headroom--unpinned");
            // }
            // var headroom = new Headroom(header, {
            //     tolerance: {
            //       down : 0,
            //       up : 0
            //     },
            //     offset : 100,
            //     classes: {
            //       initial: "animated",
            //       pinned: "fadeInDown",
            //       unpinned: "fadeOutUp",
            //       onUnpin : function() {
            //         console.log("unpinned");
            //       }
            //     }
            // });
            // headroom.init();
        }());

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
