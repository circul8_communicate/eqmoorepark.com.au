<?php
/**
 * Template Name: Page - Explore EQ
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
<?php endwhile; ?>

<div class="container my-5">
    <div class="row">
        <div class="col-lg-7 mb-3 mb-lg-0">
            <?php while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
        </div>
        <div class="col-lg-4 offset-lg-1">
            <div class="explore-eq-search">
                <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                    <button type="submit" class="search-submit"><?php echo esc_attr_x( 'Search', 'submit button' ) ?></button>
                    <label for="s" class="w-100 mb-0">
                        <span class="sr-only"><?php echo _x( 'Search for:', 'label' ) ?></span>
                        <input type="search" class="search-field w-100" value="<?php echo get_search_query() ?>" name="s" id="s"  required/>
                    </label>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
    $directories = new WP_Query( (
        array(
            'post_type' => 'directory',
            'orderby'=> 'title',
            'order' => 'ASC',
            'posts_per_page' => -1
        )
    ) );
?>
<?php if( $directories->have_posts() ): ?>
<div id="jsCardsEntry" class="py-5 bg-gold cards-grid">
    <div class="container">
        <div class="cards-filter mb-5">
            <?php $directory_categories = get_terms(array('taxonomy' => 'directory-category', 'orderby'=> 'title', 'parent' => '0','hide_empty' => false)); ?>
            <?php if ( ! empty( $directory_categories ) && ! is_wp_error( $directory_categories ) ): ?>
                <div class="d-none d-lg-block">
                    <div id="jsFilters" class="row">
                        <?php foreach ( $directory_categories as $directory_category ): ?>
                            <div class="col-lg-2 text-center mb-3">
                                <button data-filter=".<?php echo $directory_category->slug; ?>" class="btn btn-light btn-narrow btn-block hover-primary"><?php echo $directory_category->name; ?></button>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="d-block d-lg-none">
                    <div id="jsSelectFilters">
                        <select class="filter-select form-control custom-select">
                            <option selected value="*">Filter</option>
                            <?php foreach ( $directory_categories as $directory_category ): ?>
                                <option value=".<?php echo $directory_category->slug; ?>"><?php echo $directory_category->name; ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div id="jsDirectories" class="row">
            <?php while( $directories->have_posts() ):  $directories->the_post(); ?>

                <?php
                    $directory_categories = wp_get_post_terms($post->ID, 'directory-category', array("fields" => "all"));
                    $directory_categories_arr = array();
                    foreach ($directory_categories as $term) {
                        $directory_categories_arr[] = $term->slug;
                    }
                    $directory_categories_class_names = implode(' ', $directory_categories_arr);
                ?>

                <div class="directory-item-wrap col-lg-4 <?php echo $directory_categories_class_names; ?>">
                    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php wp_reset_postdata(); ?>
<?php endif;  ?>

<script src="<?php echo get_template_directory_uri(); ?>/assets/scripts/interactive-map.js"></script>
