<?php
/**
 * Template Name: Page - Events Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php
    $current_time = current_time( 'timestamp' );
    $current_date = date('Ymd', $current_time);

    $meta_query = array(
        'relation'      => 'OR',
        'startdate_clause' => array(
            'key'       => 'event_information_event_details_event_date_start_date',
            'compare'   => '!=',
            'value'     => '',
        ),
        'enddate_clause' => array (
            'relation' => 'AND',
            array(
                'key'        => 'event_information_event_details_event_date_start_date',
                'compare'    => '=',
                'value'      => '',
            ),
            array(
                'key'       => 'event_information_event_details_event_date_end_date',
                'compare'   => '!=',
                'value'     => '',
            ),
        ),
    );

    $nodate_meta_query = array(
        'relation' => 'AND',
        array(
            'key'        => 'event_information_event_details_event_date_start_date',
            'compare'    => '=',
            'value'      => '',
        ),
        array(
            'key'        => 'event_information_event_details_event_date_end_date',
            'compare'    => '=',
            'value'      => '',
        ),
    );
?>

<div class="container">
    <h2 class="text-uppercase mb-5">Upcoming Events</h2>
</div>
<div id="jsCardsEntry" class="cards-grid bg-primary decor-top">
    <div class="white-underlay"></div>
    <div class="container">
        <div class="row pb-4">
            <?php
                $events_with_date = new WP_Query( (
                    array(
                        'post_type' => 'event',
                        'meta_query' => $meta_query,
                        'orderby' => array(
                            'startdate_clause' => 'ASC',
                            'enddate_clause' => 'ASC',
                        ),
                        'posts_per_page' => -1,
                    )
                ) );

                $events_with_nodate = new WP_Query( (
                    array(
                        'post_type' => 'event',
                        'meta_query' => $nodate_meta_query,
                        'order' => 'ASC',
                        'orderby' => 'title',
                        'posts_per_page' => -1,
                    )
                ) );
            ?>
            <?php if( $events_with_date->have_posts() ): ?>
                <?php while( $events_with_date->have_posts() ):  $events_with_date->the_post(); ?>

                    <div class="col-lg-4 d-flex flex-column">
                        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif;  ?>

            <?php if( $events_with_nodate->have_posts() ): ?>
                <?php while( $events_with_nodate->have_posts() ):  $events_with_nodate->the_post(); ?>
                    <div class="col-lg-4 d-flex flex-column">
                        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
        <!-- <div class="text-center mb-5 fadein-up">
            <a href="#" class="btn btn-link btn-link_center text-uppercase text-white">Load More</a>
        </div> -->
    </div>
</div>


