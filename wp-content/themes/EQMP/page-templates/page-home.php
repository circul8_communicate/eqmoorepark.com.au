<?php
/**
 * Template Name: Page - Home Page
 */
?>

<!-- HOME HERO -->
<?php if($hero_section = get_field('hero_section')): ?>
    <section>
        <div class="row no-gutters">
            <div class="col-lg-7">
                <div class="hero-slider">
                    <?php if($hero_sliders = $hero_section['hero_slider']): ?>
                    <div id="jsHeroImagesSlider" class="hero-slider__slides">
                        <?php foreach($hero_sliders as $hero_slider ): ?>
                            <div class="section-image bg-gold">
                                <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $hero_slider['slider_image']['sizes']['large']; ?>)"></figure>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div id="jsHeroCopiesSlider" class="hero-slider__copy mb-0">
                        <?php foreach($hero_sliders as $hero_slider ): ?>
                            <?php if($hero_slider['slider_copy']['call_to_action']['link']): ?>
                                <a href="<?php echo $hero_slider['slider_copy']['call_to_action']['link']; ?>" title="<?php echo $hero_slider['slider_copy']['call_to_action']['title']; ?>" class="h-100 w-100 d-flex align-items-end text-white">
                                    <div>
                                        <?php if($hero_slider['slider_copy']['title']): ?>
                                            <h2 class="mb-3 text-uppercase"><?php echo $hero_slider['slider_copy']['title']; ?></h2>
                                        <?php endif; ?>
                                        <?php if($hero_slider['slider_copy']['caption']): ?>
                                            <p><?php echo $hero_slider['slider_copy']['caption']; ?></p>
                                        <?php endif; ?>
                                    </div>
                                </a>
                            <?php else: ?>
                                <div class="h-100 w-100 d-flex align-items-end">
                                    <div>
                                        <?php if($hero_slider['slider_copy']['title']): ?>
                                            <h2 class="mb-3 text-uppercase"><?php echo $hero_slider['slider_copy']['title']; ?></h2>
                                        <?php endif; ?>
                                        <?php if($hero_slider['slider_copy']['caption']): ?>
                                            <p><?php echo $hero_slider['slider_copy']['caption']; ?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-5 d-flex flex-column">
                <?php if($hero_ctas = $hero_section['hero_ctas']): ?>
                    <div class="hero-ctas bg-dark flex-fill d-flex flex-column">
                        <div class="row no-gutters flex-fill">
                            <?php if($cta_item_1 = $hero_section['hero_ctas']['cta_item_1']): ?>
                                <div class="col-lg-6 d-flex flex-column">
                                    <a href="<?php echo $cta_item_1['link']; ?>" class="hero-cta text-white flex-fill d-flex flex-row flex-lg-column align-items-center justify-content-center">
                                        <div class="hero-cta__icon">
                                            <img class="img-fluid lazy" data-src="<?php echo $cta_item_1['icon']['sizes']['thumbnail']; ?>" alt="">
                                        </div>
                                        <div class="hero-cta__header">
                                            <h3 class="hero-cta__title"><?php echo $cta_item_1['title']; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <?php if($cta_item_2 = $hero_section['hero_ctas']['cta_item_2']): ?>
                                <div class="col-lg-6 d-flex flex-column">
                                    <a href="<?php echo $cta_item_2['link']; ?>" class="hero-cta text-white flex-fill d-flex flex-row flex-lg-column align-items-center justify-content-center">
                                        <div class="hero-cta__icon">
                                            <img class="img-fluid lazy" data-src="<?php echo $cta_item_2['icon']['sizes']['thumbnail']; ?>" alt="">
                                        </div>
                                        <div class="hero-cta__header">
                                            <h3 class="hero-cta__title"><?php echo $cta_item_2['title']; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="row no-gutters flex-fill">
                            <?php if($cta_item_3 = $hero_section['hero_ctas']['cta_item_3']): ?>
                                <div class="col-lg-6 d-flex flex-column">
                                    <a href="<?php echo $cta_item_3['link']; ?>" class="hero-cta text-white flex-fill d-flex flex-row flex-lg-column align-items-center justify-content-center">
                                        <div class="hero-cta__icon">
                                            <img class="img-fluid lazy" data-src="<?php echo $cta_item_3['icon']['sizes']['thumbnail']; ?>" alt="">
                                        </div>
                                        <div class="hero-cta__header">
                                            <h3 class="hero-cta__title"><?php echo $cta_item_3['title']; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>
                            <?php if($cta_item_4 = $hero_section['hero_ctas']['cta_item_4']): ?>
                                <div class="col-lg-6 d-flex flex-column">
                                    <a href="<?php echo $cta_item_4['link']; ?>" class="hero-cta text-white flex-fill d-flex flex-row flex-lg-column align-items-center justify-content-center">
                                        <div class="hero-cta__icon">
                                            <img class="img-fluid lazy" data-src="<?php echo $cta_item_4['icon']['sizes']['thumbnail']; ?>" alt="">
                                        </div>
                                        <div class="hero-cta__header">
                                            <h3 class="hero-cta__title"><?php echo $cta_item_4['title']; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- END: HOME HERO -->

<?php get_template_part('templates/repeatable-sections'); ?>

<?php if(($upcoming_events_section = get_field('upcoming_events_section')) && ($upcoming_events_section['display_section'] === 'yes')): ?>
    <?php get_template_part('templates/section', 'upcoming-events'); ?>
<?php endif; ?>

<?php if(($instagram_feed_section = get_field('instagram_feed_section')) && ($instagram_feed_section['display_section'] === 'yes')): ?>
    <?php get_template_part('templates/section', 'instagram-gallery'); ?>
<?php endif; ?>

<?php if(($newsletter_signup_section = get_field('newsletter_signup_section')) && ($newsletter_signup_section['display_section'] === 'yes')): ?>
    <?php get_template_part('templates/section', 'newsletter-signup'); ?>
<?php endif; ?>
