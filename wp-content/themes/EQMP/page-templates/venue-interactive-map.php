<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php echo htmlentities($_GET['title']) ?></title>
  <script
  src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
  integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
  crossorigin="anonymous"></script>
  <style type="text/css">
  html, body { margin: 0; padding: 0; }
  </style>
</head>
<body>
<div id="map" style="position: relative; width: 100vw; height: 100vh;"></div>
<script type="text/javascript" src=" https://maps.abuzzinteractive.net/entertainmentQuarter/api/v1.52/abuzz/w/abuzzWebAPI.js"></script>
<script>
	ABUZZ.setAbuzzPath('https://maps.abuzzinteractive.net/entertainmentQuarter/api/v1.52/abuzz/');
	var el = document.querySelector('#map');
	if ('<?php echo htmlentities($_GET['id']); ?>' !== '') {
		ABUZZ.showMapWithIDest(el, '<?php echo htmlentities($_GET['id']); ?>', '', {showPopup: true});
	} else {
		ABUZZ.showMapWithLevelByExtID(el, 'G', {zoom: 0, showPopup: true});
	}
</script>
</body>
</html>
