<?php
/**
 * Template Name: Page - Venue Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<div id="jsCardsEntry" class="cards-grid bg-navy">
    <div class="white-underlay"></div>
    <div class="container">
        <div class="row pb-4">
            <?php $venue_categories = get_terms( array( 'taxonomy' => 'venue-category') ); ?>
            <?php foreach($venue_categories as $venue_category): ?>
                <div class="col-lg-4 d-flex flex-column">
                    <article <?php post_class('entry-card flex-fill d-flex flex-column'); ?>>
                        <div class="entry-card-thumbnail">
                            <?php $featured_image = get_field('featured_image', 'term_' . $venue_category->term_id); ?>
                            <figure class="bg-image bg-image_cover lazy" data-bg="url(<?php echo $featured_image['sizes']['large']; ?>)"></figure>
                        </div>

                        <div class="entry-card-content">
                            <header class="entry-card-header">
                                <h2 class="entry-card-title mb-3"><a href="<?php echo get_term_link($venue_category->term_id); ?>"><?php echo $venue_category->name; ?></a></h2>
                            </header>
                            <div class="entry-card-summary">
                                <?php if($venue_description = $venue_category->description): ?>
                                    <?php echo wp_trim_words( $venue_description, 20, '...' ); ?>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="entry-card-footer mt-auto">
                            <a class="btn btn-gold btn-block" href="<?php echo get_term_link($venue_category->term_id); ?>">Read More</a>
                        </div>
                    </article>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
